const {Router} = require('express')
const router = Router()

var axios = require('axios')
var fd = require('form-data')


router.post('/apifacial', (req, res)=>{
    var data = new fd()
    //data.append('apiKey', 'adbe93f0e3b84cdeb182036c1737187d')
    
    let ans

    const {id_front, id_back, selfie, ambient} = req.body
    data.append('id_front', id_front)
    data.append('id_back', id_back)
    data.append('selfie', selfie)
    //data.append('apiKey', 'adbe93f0e3b84cdeb182036c1737187d')
    if(ambient == 0){
        data.append('apiKey', '0dd773a7496e48dd81cf3bb56c139e5e')
        var config= {
            method: 'post',
            url: 'https://dev-api.7oc.cl/v2/face-and-document',
            headers: {
                ...data.getHeaders()
            },
            data: data
        };
    }
    else if(ambient == 1){
        data.append('apiKey', 'adbe93f0e3b84cdeb182036c1737187d')
        var config= {
            method: 'post',
            url: 'https://sandbox-api.7oc.cl/v2/face-and-document',
            headers: {
                ...data.getHeaders()
            },
            data: data
        };
    }
    else if(ambient == 2){
        data.append('apiKey', 'f70ec70d626e4f9cabc8efe3267c8742')
        var config= {
            method: 'post',
            url: 'https://prod-api.7oc.cl/facial/face-and-document/v3',
            headers: {
                ...data.getHeaders()
            },
            data: data
        };
    }

    data.append('documentType', 'CHL2')
    data.append('sign_extract', 'false')


    axios(config)
    .then(function (response) {
    ans = response.data
    res.json(ans)
    })
    .catch(function (error) {
    console.log(error)
    });
});

module.exports = router