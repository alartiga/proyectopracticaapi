const {Router} = require('express')
const router = Router()

const axios = require('axios');
const FormData = require('form-data');

router.post('/splunk', async (req, res)=>{
  var ans
    let data = new FormData();
    data.append('environment', 'explotacion');

    const {session_id, ambient} = req.body
    var aux = JSON.stringify(session_id)
    aux = aux.replace(/\"/g, "")

    console.log("ambient is: " + ambient)

    if(ambient == 0) data.append('query', 'index="api_dev" ' + aux);
    else if(ambient == 1) data.append('query', 'index="api_sandbox" ' + aux);
    else if(ambient == 2) data.append('query', 'index="api_prod" ' + aux);

    //data.append('query', aux);
    data.append('offset', '0');
    data.append('limit', '5');
    let config = {
        method: 'post',
        url: 'https://micro-services.toc.ai/splunk-api/report',
        headers: { 
          'Authorization': 'eyJpdiI6IkxGVDBqRGRvdEhJRVJrRXlxQ0dyakE9PSIsInZhbHVlIjoiZms2SVhuZmEwZ2tDXC9CNnlRZjhxR29nSzUwXC94NU9EK2lualkyNmYrS3JrPSIsIm1hYyI6ImI0NGEzZjBmM2UwYmE1ZDVmZWYzNTUwMmJiYmNiMjEzOGRmNTNlZGE5NjU4NDlmOWZjN2VmMGZiZThiMDA0ZjUifQ==',
          ...data.getHeaders()
        },
        data : data
      };


    try{
      const response = await axios(config)

      ans = response.data.results[0]._raw
      res.status(200).json(ans)

    }catch(error){
      console.log(error)
    }

      res.end()
      /*

    axios(config)
    .then(function (response) {
    ans = response.data.results[0]
    
    //var sp = ans.split(" ")
    //console.log(sp[5] + sp[6] + sp[7])
    res.json(ans._raw)
    })
    .catch(function (error) {
    console.log(error);
    });
    */
})

module.exports = router