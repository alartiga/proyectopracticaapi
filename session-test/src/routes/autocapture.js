const {Router} = require('express')
const router = Router()

var axios = require('axios')
var fd = require('form-data')

var ans

router.post('/autocapture', async (req, res) => {
    var data = new fd()
    
    
    const {autocapture, ambient} = req.body
    if(autocapture) {
        
        data.append('autocapture', autocapture)
        if(ambient == 0){
            data.append('apiKey', '0dd773a7496e48dd81cf3bb56c139e5e')
            var config= {
                method: 'post',
                url: 'https://dev-api.7oc.cl/session-manager/v1/session-id',
                headers: {
                    ...data.getHeaders()
                },
                data: data
            };
        }
        else if(ambient == 1){
            data.append('apiKey', 'adbe93f0e3b84cdeb182036c1737187d')
            var config= {
                method: 'post',
                url: 'https://sandbox-api.7oc.cl/session-manager/v1/session-id',
                headers: {
                    ...data.getHeaders()
                },
                data: data
            };
        }
        else if(ambient == 2){
            data.append('apiKey', 'f70ec70d626e4f9cabc8efe3267c8742')
            var config= {
                method: 'post',
                url: 'https://prod-api.7oc.cl/session-manager/session-id/v1',
                headers: {
                    ...data.getHeaders()
                },
                data: data
            };
        }


        try{
            const resp = await axios(config)
            res.status(200).json(resp.data)
        }catch(error){
            console.log(error)
        }

        res.end()
/*
        axios(config)
        .then(function (response) {
        //ans = response.data
        res.status(200).json(response.data)
        
        res.end()
        })
        .catch(function (error) {
        console.log(error)
        });
*/
    } else {
        res.status(500).json({error: "Hubo un error."})
    }
});

module.exports = router