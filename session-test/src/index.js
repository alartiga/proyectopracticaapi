const express = require('express')
const cors = require('cors')
const app = express()
const morgan = require('morgan')

app.use(cors())

//settings
app.set('port', process.env.PORT || 3000)
app.set('json spaces', 2)

//middleware
app.use(morgan('dev'))
app.use(express.urlencoded({extended: false}))
app.use(express.json())

//routes
app.use(require('./routes/autocapture'))
app.use(require('./routes/liveness'))
app.use(require('./routes/fvd'))
app.use(require('./routes/apifacial'))
app.use(require('./routes/splunk'))


//start
app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`)
});

/*
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';

const app = express()

app.use(cors())
//import autocaptureRoute from './routes/autocapture';
//import livenessRoute from './routes/liveness';
//import fvdRoute from './routes/fvd';
//import apiFRoute from './routes/apifacial';

//settings
app.set('port', process.env.PORT || 3000)
app.set('json spaces', 2)

//middleware
app.use(morgan('dev'))
app.use(express.urlencoded({extended: false}))
app.use(express.json())

app.use('./routes/autocapture', autocaptureRoute)
app.use('./routes/liveness', livenessRoute)
app.use('./routes/fvd', fvdRoute)
app.use('./routes/apifacial', apiFRoute)


//start
app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`)
});

//*/